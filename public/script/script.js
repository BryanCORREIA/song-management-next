$(document).ready(function($){
  $('.login_box').on('click', function(event){
    $('.main-nav').children('ul').removeClass('is-visible');
    $('.cd-user-modal').addClass('is-visible');
    login_selected();
  });

  $('.cd-user-modal').on('click', function(event){
    if( $(event.target).is($('.cd-user-modal')) || $(event.target).is('.cd-close-form') ) {
      $('.cd-user-modal').removeClass('is-visible');
    }
  });

  $(document).keyup(function(event){
    if(event.which=='27'){
      $('.cd-user-modal').removeClass('is-visible');
    }
  });

  $('.cd-switcher').on('click', function(event) {
    event.preventDefault();
    ( $(event.target).is( $('.cd-switcher').children('li').eq(0).children('button') ) ) ? login_selected() : signup_selected();
  });

  $('.hide-password').on('click', function(){
    var $this= $(this),
      $password_field = $this.prev('input');

    ( 'password' == $password_field.attr('type') ) ? $password_field.attr('type', 'text') : $password_field.attr('type', 'password');
    ( '<i class="far fa-eye-slash"></i>' === $this.html())  ? $this.html('<i class="far fa-eye"></i>') : $this.html('<i class="far fa-eye-slash"></i>');
    $password_field.putCursorAtEnd();
  });

  $('.cd-user-modal').find('#cd-login').find('.cd-form-bottom-message a').on('click', function(event){
    event.preventDefault();
    forgot_password_selected();
  });

  $('.cd-user-modal').find('#cd-reset-password').find('.cd-form-bottom-message a').on('click', function(event){
    event.preventDefault();
    login_selected();
  });

  function login_selected(){
    $('.cd-user-modal').find('#cd-login').addClass('is-selected');
    $('.cd-user-modal').find('#cd-signup').removeClass('is-selected');
    $('.cd-user-modal').find('#cd-reset-password').removeClass('is-selected');
    $('.cd-switcher').children('li').eq(0).children('button').addClass('selected');
    $('.cd-switcher').children('li').eq(1).children('button').removeClass('selected');
  }

  function signup_selected(){
    $('.cd-user-modal').find('#cd-login').removeClass('is-selected');
    $('.cd-user-modal').find('#cd-signup').addClass('is-selected');
    $('.cd-user-modal').find('#cd-reset-password').removeClass('is-selected');
    $('.cd-switcher').children('li').eq(0).children('button').removeClass('selected');
    $('.cd-switcher').children('li').eq(1).children('button').addClass('selected');
  }

  function forgot_password_selected(){
    $('.cd-user-modal').find('#cd-login').removeClass('is-selected');
    $('.cd-user-modal').find('#cd-signup').removeClass('is-selected');
    $('.cd-user-modal').find('#cd-reset-password').addClass('is-selected');
  }
});


jQuery.fn.putCursorAtEnd = function() {
  return this.each(function() {
    if (this.setSelectionRange) {
      var len = $(this).val().length * 2;
      this.setSelectionRange(len, len);
    } else {
      $(this).val($(this).val());
    }
  });
};
