import { Album } from '../../dto/album.model';
import SongsComponent from '../Songs/songs.component';
import { useEffect, useState } from 'react';
import Link from 'next/link';

type AlbumsComponentProps  = {
  albums: Album[];
  selectedAlbum: Album|null;
}

export default function ListAlbumsComponent(props : AlbumsComponentProps) {
  const [selectedAlbum, setSelectedAlbum] = useState<Album>();

  useEffect(() => {
    if (props.selectedAlbum != null) {
      setSelectedAlbum(props.selectedAlbum);
    }
  });

  return (
    <div className="albums_container">
      <div className="albums_list">
        {props.albums.map((album: Album, index: number)=> (
          <Link href={`/albums/${album.slug}`}>
            <div key={index} className="album_content">
              <style jsx>
                {`
                .album_content {
                  background-image: url('${album.cover}');
                }
              `}
              </style>
              <div className="info_album">
                <p className="title_album">{album.title}</p>
                <p className="artist_album">{album.artist.name}</p>
                <p className="year_album">{album.year}</p>
              </div>
              <div className="number_songs">
                <span>{album.songs.length} chansons</span>
              </div>
            </div>
          </Link>
        ))}
      </div>
      { selectedAlbum &&
      <div className="aside_styles">
        <div className="selected_style">
          <p className="selected_style_name">{selectedAlbum?.title}</p>
          <hr />
          {selectedAlbum?.songs.length > 0 &&
            <SongsComponent songs={selectedAlbum?.songs}/>
          }
          {selectedAlbum?.songs.length == 0 &&
          <p className="no_songs">Cet album ne contient aucune musique.</p>
          }
        </div>
      </div>
      }
    </div>
  )
}
