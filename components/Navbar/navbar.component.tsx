import React, { FunctionComponent, useEffect, ReactNode, useState } from 'react';
import ActiveLink from '../ActiveLink/activeLink.component';
// @ts-ignore
import cookie from 'js-cookie';
import PlaylistNavComponent from '../Playlist/Nav/playlistnav.component';
import cookies from 'next-cookies';
import { useRecoilState } from 'recoil';
import { userConnected } from '../userConnected.state';
import { useRouter } from 'next/router';

export default function NavbarComponent(props: any) {
  const [isAdmin, setIsAdmin] = useState<boolean>(false);
  const [userSession, setUserSession] = useRecoilState(userConnected);
  const router = useRouter();

  const jwt = cookie.get('jwt');

  async function loadCurrentUser() {
    const userReq = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}user/profil`, {
      method: 'GET',
      headers: new Headers({
        'Authorization': 'Bearer ' + jwt
      })
    });

    const user = await userReq.json();

    if (user.statusCode == 401) { cookie.remove('jwt'); setIsAdmin(false); return null; }

    setIsAdmin(user.role == "ROLE_ADMIN");
  }

  function logout() {
    cookie.remove('jwt');
    // @ts-ignore
    setUserSession(false);
    router.push('/');
  }

  useEffect(() => {
    loadCurrentUser();
  }, [userSession]);

  return (
    <nav>
      <div className="logo_box">
        <img src="/res/logo.svg" alt=""/>
      </div>
      {isAdmin &&
        <div className="section_nav" id="admin_section">
          <p className="section_nav_title">Administration</p>

          <ActiveLink activeClassName='active' href='/administration'>
            <a>
              <i className="fas fa-sign-out"></i>
              Panneau de gestion
            </a>
          </ActiveLink>
        </div>
      }
      <div className="section_nav">
        <p className="section_nav_title">Menu</p>
        <ActiveLink activeClassName='active' href='/'>
          <a>
            <i className="fas fa-home-lg-alt"></i>
            Accueil
          </a>
        </ActiveLink>
        <ActiveLink activeClassName='active' href='/musiques'>
          <a>
            <i className="fas fa-music"></i>
            Musiques
          </a>
        </ActiveLink>
        <ActiveLink activeClassName='active' href='/artistes'>
          <a>
            <i className="fas fa-user-music"></i>
            Artistes
          </a>
        </ActiveLink>
        <ActiveLink activeClassName='active' href='/albums'>
          <a>
            <i className="fas fa-album-collection"></i>
            Albums
          </a>
        </ActiveLink>
      </div>
      { jwt &&
          <PlaylistNavComponent />
      }
      <div className="section_nav" id="general_section">
        <p className="section_nav_title">Général</p>
        <ActiveLink activeClassName='active' href='/parametres'>
          <a>
            <i className="fas fa-cog"></i>
            Paramètres
          </a>
        </ActiveLink>
        {jwt &&
            <a onClick={logout} className="cursor_pointer">
              <i className="fas fa-sign-out"></i>
              Déconnexion
            </a>
        }
      </div>
    </nav>
  );
};
