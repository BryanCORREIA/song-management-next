import { atom } from 'recoil';

const userConnected = atom({
  key: 'userConnected',
  default: false
})

export { userConnected }
