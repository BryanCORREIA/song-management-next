import { Router, useRouter } from 'next/router';
import Link from 'next/link';
import { Song } from '../../dto/song.model';
import { useEffect, useState } from 'react';
import SongsComponent from '../Songs/songs.component';
import { Style } from '../../dto/style.model';

type IStylesProps  = {
  styles: Style[];
  styleSelected: Style|null;
}

export default function StylesComponent(props: IStylesProps) {
  const [selectedStyle, setSelectedStyle] = useState<Style>();
  const router = useRouter();

  useEffect(() => {
    if (props.styleSelected != null) {
      setSelectedStyle(props.styleSelected);
    }
  });

  return (
    <div className="musiques_box extend">
      <div className="styles_song extend">
        {props.styles.map((style: Style, index: number)=> (
          <Link href={`/musiques/${style.slug}`}>
            <div key={style.id} className="style_box" >
              <div className={selectedStyle?.id == style.id ? "style_song active" : "style_song"}>
                <div className="illus_style">
                  <i className="far fa-waveform"></i>
                </div>
                <p className="name_style">{style.name}</p>
                <div className="style_number_song"><span>{style.songs.length}</span></div>
              </div>
            </div>
          </Link>
        ))}
      </div>
      { selectedStyle &&
        <div className="aside_styles">
          <div className="selected_style">
            <h1>Style musical</h1>
            <p className="selected_style_name">{selectedStyle?.name}</p>
            <hr />
            {selectedStyle?.songs.length > 0 &&
              <SongsComponent songs={selectedStyle?.songs}/>
            }
            {selectedStyle?.songs.length == 0 &&
              <p className="no_songs">Nous n'avons aucune musique dans ce style.</p>
            }
          </div>
        </div>
      }
    </div>

  )
}
