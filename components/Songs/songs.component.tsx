import { useRouter } from 'next/router';
import Link from 'next/link';
import SongComponent from './Song/song.component';
import { Song } from '../../dto/song.model';
import { useEffect, useState } from 'react';

type SongsComponentProps  = {
  songs: Song[];
}

export default function SongsComponent(props : SongsComponentProps) {
  return (
    <div className="songs_list">
      {props.songs.map((song: Song, index: number)=> (
        <SongComponent key={song.id} song={song}/>
      ))}
    </div>
  )
}
