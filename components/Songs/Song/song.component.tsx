import { useRouter } from 'next/router';
import Link from 'next/link';
import { Song } from '../../../dto/song.model';
import { useRecoilState } from 'recoil';
import { songPlayer } from '../../states';

type SongComponentProps  = {
  song: Song;
}

export default function SongComponent(props : SongComponentProps) {
  const [currentSong, setCurrentSong] = useRecoilState(songPlayer);

  const setSong = (song: Song) => {
    setCurrentSong(song);
  };

  function convertDuration(d: number) {
    d = Number(d);
    const h = Math.floor(d / 3600);
    const m = Math.floor(d % 3600 / 60);
    const s = Math.floor(d % 3600 % 60);

    const hDisplay = h > 0 ? h + (h === 1 ? " heure, " : " heures, ") : "";
    const mDisplay = m > 0 ? m + (m === 1 ? " minute, " : " minutes, ") : "";
    const sDisplay = s > 0 ? s + (s === 1 ? " seconde" : " secondes") : "";
    return hDisplay + mDisplay + sDisplay;
  }

  return (
    <div className="song_in_list">
      <div className="left_side">
        <i className="fas fa-circle"></i>
        <p className="title_song">{props.song.title}</p>
      </div>
      { props.song.src &&
        <div className="right_side">
          <p className="duration_song">{ convertDuration(props.song.duration) }</p>
          <i className="far fa-play-circle" onClick={() => setSong(props.song)}></i>
        </div>
      }
    </div>
  )
}
