import styles from '../../styles/Home.module.css';
import Head from 'next/head';
import NavbarComponent from '../Navbar/navbar.component';
import HeaderBarComponent from '../HeaderBar/header.component';
import AudioPlayerComponent from '../AudioPlayer/audio.component';
import { useRecoilState } from 'recoil';
import { songPlayer } from '../states';
import { userConnected } from '../userConnected.state';
import { useEffect } from 'react';


// @ts-ignore
export default function LayoutBase({ children }) {
  const currentSong = useRecoilState(songPlayer)[0];
  const userSession = useRecoilState(userConnected)[0];

  useEffect(() => {}, [userSession]);

  return (
    <div className={styles.container}>
      <Head>
        <title>Music'All</title>
        <meta name="description" content="Music'All" />
        <link rel="icon" href="/res/logo_small_icon_only_inverted.png" type="image/png"/>
        <link rel="stylesheet" href="/lib/fontawesome/css/all.css"/>
        <script
          src="https://code.jquery.com/jquery-3.6.0.min.js"
          integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
          crossOrigin="anonymous" />
        <script
          src="/script/script.js" />
        <script
          src="/script/upload.js" />

      </Head>

      <div className={styles.mainContainer}>
        <NavbarComponent />

        <main>
          <HeaderBarComponent />
          { children }
        </main>
      </div>

      <footer>
        {currentSong &&
          <AudioPlayerComponent />
        }
      </footer>
    </div>
  );
};
