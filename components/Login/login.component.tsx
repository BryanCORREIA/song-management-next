import React, { useEffect, useState } from 'react';
import { useCookies } from 'react-cookie';
// @ts-ignore
import cookie from 'js-cookie';
import { useRecoilState } from 'recoil';
import { userConnected } from '../userConnected.state';

export default function LoginComponent() {
  const [name, setName] = useState<string>();
  const [cookieR, setCookie] = useCookies(["jwt"]);
  const [formError, setFormError] = useState<boolean>(false);
  const [userSession, setUserSession] = useRecoilState(userConnected);

  const jwt = cookie.get('jwt');

  async function loadCurrentUser() {
    if (cookieR.jwt) {
      const userReq = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}user/profil`, {
        method: 'GET',
        headers: new Headers({
          'Authorization': 'Bearer ' + cookieR.jwt
        })
      });

      const user = await userReq.json();

      if (user.statusCode == 401) { cookie.remove('jwt'); return null; }

      setName(user.firstname + ' ' + user.lastname);
    } else {
      setName('');
    }
  }

  const handleLoginSubmit = async (event: any) => {
    event.preventDefault();

    const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}auth/login`, {
      body: JSON.stringify({
        username: event.target.username.value,
        password: event.target.password.value
      }),
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST'
    });

    const result = await res.json();

    if (result.statusCode == 401) {
      setFormError(true);
      event.target.password.value = "";
    } else if (result.access_token) {
      setFormError(false);
      setCookie("jwt", result.access_token, {
        path: "/",
        maxAge: 3600, // Expires after 1hr
        sameSite: true,
      });

      // @ts-ignore
      setUserSession(true);
    }
  }

  const handleSignupSubmit = async (event: any) => {
    event.preventDefault();

    const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}auth/signup`, {
      body: JSON.stringify({
        username: event.target.username.value,
        firstname: event.target.firstname.value,
        lastname: event.target.lastname.value,
        password: event.target.password.value,
      }),
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST'
    });

    const result = await res.json();

    if (result.statusCode == 401) {
      setFormError(true);
      event.target.password.value = "";
    } else if (result.access_token) {
      setFormError(false);
      setCookie("jwt", result.access_token, {
        path: "/",
        maxAge: 3600, // Expires after 1hr
        sameSite: true,
      });
      // @ts-ignore
      setUserSession(true);
    }
  }

  useEffect(() => {
    loadCurrentUser();
  }, [cookieR, jwt]);

  return (
    <div>
      <div className="login_box">
        <div className="user">
          <div className="user_status">
            <i className="fad fa-user-circle"></i>
            <span>{jwt ? name : 'Connexion' }</span>
          </div>
        </div>
      </div>

      {!jwt &&
        <div className="cd-user-modal">
        <div className="cd-user-modal-container">
          <ul className="cd-switcher">
            <li><button role="button">Connexion</button></li>
            <li><button role="button">Inscription</button></li>
          </ul>

          <div id="cd-login">
            <form className="cd-form" onSubmit={handleLoginSubmit}>
              <p className="fieldset">
                <label className="image-replace cd-email" htmlFor="signin-email">Adresse mail</label>
                <input className="full-width has-padding has-border" id="signin-email" type="email"
                       placeholder="Adresse mail" name="username"/>
              </p>

              <p className="fieldset">
                <label className="image-replace cd-password" htmlFor="signin-password">Mot de passe</label>
                <input className="full-width has-padding has-border" id="signin-password" type="password"
                       placeholder="Mot de passe" name="password"/>
                <a href="#0" className="hide-password"><i className="far fa-eye"></i></a>
              </p>

              <p className="fieldset">
                <input className="full-width" type="submit" value="Se connecter" />
              </p>

              <p className="cd-form-bottom-message"><a href="#0">Mot de passe oublié ? Cliquez ici</a></p>
            </form>
          </div>

          <div id="cd-signup">
            <form className="cd-form" onSubmit={handleSignupSubmit}>
              <div className="flex-container">
                <p className="fieldset">
                  <label className="image-replace cd-username" htmlFor="signup-firstname">Prénom</label>
                  <input className="full-width has-padding has-border" id="signup-firstname" type="text"
                         placeholder="Prénom" name="firstname"/>
                </p>

                <p className="fieldset">
                  <label className="image-replace cd-username" htmlFor="signup-lastname">Nom</label>
                  <input className="full-width has-padding has-border" id="signup-lastname" type="text"
                         placeholder="Nom" name="lastname"/>
                </p>
              </div>

              <p className="fieldset">
                <label className="image-replace cd-email" htmlFor="signup-email">Adresse mail</label>
                <input className="full-width has-padding has-border" id="signup-email" type="email"
                       placeholder="Adresse mail" name="username"/>
              </p>

              <p className="fieldset">
                <label className="image-replace cd-password" htmlFor="signup-password">Mot de passe</label>
                <input className="full-width has-padding has-border" id="signup-password" type="password"
                       placeholder="Mot de passe" name="password"/>
                <a href="#0" className="hide-password"><i className="far fa-eye"></i></a>
              </p>

              <p className="fieldset">
                <input className="full-width has-padding" type="submit" value="S'incricre" />
              </p>
            </form>
          </div>

          <div id="cd-reset-password">
            <p className="cd-form-message">Vous avez perdu votre mot de passe ? Entrez votre adresse mail. Vous recevrez un lien pour recréer vote mot de passe.</p>

            <form className="cd-form">
              <p className="fieldset">
                <label className="image-replace cd-email" htmlFor="reset-email">Adresse mail</label>
                <input className="full-width has-padding has-border" id="reset-email" type="email" placeholder="Adresse mail" />
              </p>

              <p className="fieldset">
                <input className="full-width has-padding" type="submit" value="Rénitialiser mon mot de passe" />
              </p>
            </form>

            <p className="cd-form-bottom-message"><a href="#0">Retour à la connexion</a></p>
          </div>
          <a href="#0" className="cd-close-form">Fermer</a>
        </div>
      </div>
      }
    </div>
  );
};
