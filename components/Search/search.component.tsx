import React, { FunctionComponent, useEffect, ReactNode, useState } from 'react';
import { useRouter } from 'next/router';

export default function SearchComponent() {
  const routeur = useRouter();
  const [search, setSearch] = useState<string>();

  async function handleKeyDown(event: any) {
    if (event.key === 'Enter') {
      await routeur.push(`/resultat?recherche=${search}`);
    }
  }

  function handleChange(e: any) {
    setSearch(e.target.value);
  }

  return (
    <div className="search_bar">
      <i className="far fa-search"></i>
      <input
        type="text"
        placeholder="Rechercher une musique, un artiste, un album"
        onKeyDown={handleKeyDown}
        onChange={handleChange}
      />
    </div>
  );
};
