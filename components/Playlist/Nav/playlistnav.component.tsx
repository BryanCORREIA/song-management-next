import React, { useEffect, useRef, useState } from 'react';
import ActiveLink from '../../ActiveLink/activeLink.component';
// @ts-ignore
import cookie from 'js-cookie';
import { Playlist } from '../../../dto/playlist.model';

export default function PlaylistNavComponent() {
  const [playlist, setPlaylist] = useState<Playlist[]>([]);
  const [playlistName, setPlaylistName] = useState<string>("");
  const [addPlaylist, setAddPlaylist] = useState<boolean>(false)
  const jwt = cookie.get('jwt');
  const addPlaylistInput = useRef(null);

  async function loadPlaylist() {
    const playlistRep = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}playlist`, {
      method: 'GET',
      headers: new Headers({
        'Authorization': 'Bearer ' + jwt
      })
    });

    const _playlist = await playlistRep.json();

    setPlaylist(_playlist);
  }

  function wantAddPlaylist() {
    setAddPlaylist(true);
    if (addPlaylistInput.current != null) {
      // @ts-ignore
      addPlaylistInput.current.focus();
    }
  }

  async function focusOut() {
    // @ts-ignore
    if(playlistName != "" && addPlaylistInput.current.value != "") {
      var data = new FormData();
      data.append('name', playlistName);
      await fetch(`${process.env.NEXT_PUBLIC_API_HOST}playlist`, {
        method: 'POST',
        headers: new Headers({
          'Authorization': 'Bearer ' + jwt,
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify({name: playlistName})
      }).then(() => {
        setAddPlaylist(false);
        loadPlaylist();
      });
    } else {
      setPlaylistName("");
      setAddPlaylist(false);
    }
  }

  async function handleKeyDown(event: any) {
    if (event.key === 'Enter') {
      await focusOut();
    }
    else if (event.key === "Escape") {
      setPlaylistName("");
      setAddPlaylist(false);
    }
  }

  function addPlaylistChanged(e: any) {
    setPlaylistName(e.target.value);
  }

  useEffect(() => {
    loadPlaylist();
  }, []);

  return (
    <div className="section_nav" id="playlist_section">
      <p className="section_nav_title">Playlist</p>
      <button role="button" className="btn-add-playlist" onClick={wantAddPlaylist} >
        <a>
          <i className="fas fa-plus-square"></i>
          Nouveau
        </a>
      </button>
      {addPlaylist &&
        <ActiveLink activeClassName='active' href='#'>
          <a>
            <i className="fal fa-list-music"></i>
            <input type="text"
                   ref={addPlaylistInput}
                   autoFocus
                   onBlur={focusOut}
                   onChange={addPlaylistChanged}
                   onKeyDown={handleKeyDown}
                   className="input-add-playlist"
            />
          </a>
        </ActiveLink>
      }
      {playlist.map((_playlist: Playlist, index: number)=> (
        <ActiveLink activeClassName='active' href={`/playlist/${_playlist.slug}`} key={index}>
          <a>
            <i className="fal fa-list-music"></i>
            {_playlist.name}
          </a>
        </ActiveLink>
      ))}
    </div>
  )
}
