import React, { FunctionComponent, useEffect, ReactNode, useRef, useState } from 'react';
import SearchComponent from '../Search/search.component';
import LoginComponent from '../Login/login.component';
import { func } from 'prop-types';
import { useRecoilState } from 'recoil';
import { songPlayer } from '../states';
import { spans } from 'next/dist/build/webpack/plugins/profiling-plugin';
import { Song } from '../../dto/song.model';

export default function AudioPlayerComponent() {
  const [currentTrack, setCurrentTrack] = useState<number>();
  const [player, setPlayer] = useState<HTMLAudioElement>();
  const [volume, setVolume] = useState<number>(100);
  const [time, setCurrentTime] = useState(0);
  const currentSong: Song = useRecoilState(songPlayer)[0];

  const setSeconds = (value: any) => {
    // @ts-ignore
    player.currentTime = value;
  };

  const pausePlayButton = () => {
    // @ts-ignore
    player.paused ? player.play() : player.pause();
  };

  const styleRange = {
    // @ts-ignore
    backgroundSize: (time) * 100 / (player ? player.duration : 100) + "% 100%",
    width: '500px'
  };

  const styleRangeVolume = {
    backgroundSize: (volume) + "% 100%",
    width: '100px'
  };

  function parseTime(time: number) {
    let s = time%60;
    const m = (time-s)/60;

    let sShow = "";

    if (s < 10) {
      sShow = '0' + s;
    }

    return m+':'+sShow;
  }

  useEffect(() => {
    if (currentSong) {
      if (player) {
        // @ts-ignore
        player.autoplay = true;

        // @ts-ignore
        player.volume = volume/100;

        // @ts-ignore
        player.ontimeupdate = () => {
          // @ts-ignore
          setCurrentTime(player.currentTime);
        };

        if (currentTrack != currentSong.id) {
          setCurrentTrack(currentSong.id);
          // @ts-ignore
          player.setAttribute('src', currentSong.src);
          // @ts-ignore
          player.load();
          // @ts-ignore
          player.play();
        }
      } else {
        setPlayer(new Audio(currentSong.src));
        setCurrentTrack(currentSong.id);
      }
    }
  });

  return (
    <div className="song_box">
      { currentSong && player != undefined &&
        <div className="flex_song">
          <div className="info_song">
            <img src={currentSong.album.cover} alt="" className="song_illus"/>
            <div className="song_info">
              <p className="title_song">{currentSong.title}</p>
              <p className="artist_song">{currentSong.album.artist.name}</p>
            </div>
          </div>
          <div className="controls">
            <div className="btn_controls">
              <button className="seconds">
                <i className="far fa-undo-alt"></i>
                <span>10</span>
              </button>
              <button onClick={pausePlayButton}>{player.paused ? <i className="far fa-play"></i> : <i className="far fa-pause"></i>}</button>
              <button className="seconds">
                <i className="far fa-redo-alt"></i>
                <span>10</span>
              </button>
            </div>
            <div className="slider_time">
              <span>{ parseTime(Math.round(time)) }</span>
              <input type="range" id="slider" min="0" max={player.duration} onChange={e => setSeconds(e.target.value)} value={player.currentTime} style={styleRange} />
              <span>{ parseTime(Math.round(player.duration)) }</span>
            </div>
          </div>
          <div className="control_audio">
            <i className="far fa-volume-up"></i>
            <input type="range" id="slider_volume" min="0" max="100" onChange={e => setVolume(parseInt(e.target.value))} value={volume} step="1" style={styleRangeVolume}/>
          </div>
        </div>
      }
    </div>
  );
};
