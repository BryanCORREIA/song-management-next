import { atom } from 'recoil';
import { Song } from '../dto/song.model';

const songPlayer = atom({
  key: 'songPlayer',
  default: Song
});

export { songPlayer }
