import React from 'react';
import SearchComponent from '../Search/search.component';
import LoginComponent from '../Login/login.component';

export default function HeaderBarComponent() {
  return (
    <div className="header_bar">
      <SearchComponent />
      <LoginComponent />
    </div>
  );
};
