import { useRouter } from 'next/router';
import Link from 'next/link';
import StylesComponent from '../../../components/Styles/styles.component';
import { Style } from '../../../dto/style.model';

type IMusiquesProps  = {
  styles: Style[];
  style: Style;
}

export default function MusiquesStyleId(props : IMusiquesProps) {
  return (
    <div>
      <h1>Musiques</h1>
      <StylesComponent
        styles={props.styles}
        styleSelected={props.style}
      />
    </div>
  )
}

export async function getServerSideProps(context: any) {
  const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}styles`)
  const styles : Style[] = await res.json()
  if (!styles) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }

  const selectedStyle = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}styles/${context.query.styleSlug}`)
  const style : Style = await selectedStyle.json()
  if (!style) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }

  return {
    props: {
      styles,
      style
    },
  }

}

