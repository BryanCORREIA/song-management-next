import { useRouter } from 'next/router';
import Link from 'next/link';
import StylesComponent from '../../components/Styles/styles.component';
import { Style } from '../../dto/style.model';

type IMusiquesProps  = {
  styles: Style[];
}

export default function Musiques(props : IMusiquesProps) {
  return (
    <div>
      <h1>Musiques</h1>
      <StylesComponent styles={props.styles} styleSelected={null}/>
    </div>
  )
}

export async function getServerSideProps(context: any) {
  const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}styles`)
  const styles : Style[] = await res.json()
  if (!styles) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }
  return {
    props: { styles }, // will be passed to the page component as props
  }

}

