import nextConnect from 'next-connect';
// @ts-ignore
import multer from 'multer';

const upload = multer({
  storage: multer.diskStorage({
    destination: './public/covers',
    filename: (req: any, file: { originalname: any; }, cb: (arg0: null, arg1: any) => any) => cb(null, file.originalname),
  }),
});

const apiRoute = nextConnect({
  onError(error, req, res) {
    // @ts-ignore
    res.status(501).json({ error: `Sorry something Happened! ${error.message}` });
  },
  onNoMatch(req, res) {
    // @ts-ignore
    res.status(405).json({ error: `Method '${req.method}' Not Allowed` });
  },
});

apiRoute.use(upload.array('coverFile'));

apiRoute.post((req, res) => {
  // @ts-ignore
  res.status(200).json({ data: 'success' });
});

export default apiRoute;

export const config = {
  api: {
    bodyParser: false, // Disallow body parsing, consume as stream
  },
};
