import { Artist } from '../../../dto/artist.model';
import { useEffect, useState } from 'react';
import Link from 'next/link';
import { isGranted } from '../../../helpers/GrantedAccess';
// @ts-ignore
import cookie from 'js-cookie';
import { router } from 'next/client';
import { useRouter } from 'next/router';

type IArtistesProps  = {
  artists: Artist[];
}

export default function Artistes(props : IArtistesProps) {
  const [listArtist, setListArtist] = useState<Artist[]>(props.artists);
  const jwt = cookie.get('jwt');
  const router = useRouter();

  async function loadArtists() {
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}artists`);
    setListArtist(await res.json());
  }

  async function deleteArtist(id: string) {
    await fetch(`${process.env.NEXT_PUBLIC_API_HOST}artists/${id}`, {
      headers: {
        'Authorization': 'Bearer ' + jwt
      },
      method: 'DELETE'
    }).then(() => {
      console.log('est');
      loadArtists();
    });
  }

  return (
    <div>
      <h1>Gestion des artistes</h1>
      <div className="btn_admin_gestion">
        <Link href="/administration">
          <a className="btn_to_panel"><i className="fad fa-arrow-left"></i> Retour au panneau de gestion</a>
        </Link>
        <Link href="/administration/artistes/creation">
          <a className="btn_to_add"><i className="fad fa-plus"></i> Ajouter un artiste</a>
        </Link>
      </div>
      <div className="table_container">
        <table className="table_admin">
          <thead>
          <tr>
            <th>Nom</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
          {listArtist.map((artist: Artist, index: number)=> (
            <tr key={index}>
              <td>{ artist.name }</td>
              <td>
                <Link href={`/administration/artistes/edition/${artist.id}`}>
                  <span className="btn-edit"><i className="fal fa-pen"></i></span>
                </Link>
                <span className="btn-delete" onClick={async () => await deleteArtist(artist.id)}><i className="far fa-trash"></i></span>
              </td>
            </tr>
          ))}
          <tr>
          </tr>
          </tbody>
        </table>
      </div>
    </div>
  )
}


export async function getServerSideProps(context: any) {
  const { isOk } = await isGranted(context);

  if (!isOk) {
    return await isGranted(context);
  }

  const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}artists`);
  const artists : Artist[] = await res.json();

  return {
    props: { artists },
  }
}
