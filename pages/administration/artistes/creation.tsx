import { UiFileInputButton } from '../../../components/File/UiFileInputButton';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { Song } from '../../../dto/song.model';
import { Style } from '../../../dto/style.model';
import { Album } from '../../../dto/album.model';
import { Artist } from '../../../dto/artist.model';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { isGranted } from '../../../helpers/GrantedAccess';
// @ts-ignore
import cookie from 'js-cookie';

export default function CreationPage() {
  const routeur = useRouter();
  const jwt = cookie.get('jwt');

  const addArtistHandler = async (event: any) => {
    event.preventDefault();

    const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}artists`, {
      body: JSON.stringify({
        name: event.target.name.value,
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + jwt
      },
      method: 'POST'
    });

    await res.json().then((res) => {
      routeur.push('/administration/artistes');
    });
  }

  useEffect(() => {}, []);

  return (
    <div>
      <h1>Ajout d'un artiste</h1>
      <div className="btn_admin_gestion">
        <Link href="/administration/artistes">
          <a className="btn_to_panel"><i className="fad fa-arrow-left"></i> Retour à la liste des artistes</a>
        </Link>
      </div>
      <div className="section_add">
        <h2>Information de l'artiste</h2>
        <div className="form_add">
          <form onSubmit={addArtistHandler}>
            <div className="form_section_flex">
              <input type="text" placeholder="Nom de l'artiste" name="name"/>
            </div>
            <div className="form_section_flex justify_content_flex_end">
              <input type="submit" value="Ajouter"/>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export async function getServerSideProps(context: any) {
  const { isOk } = await isGranted(context);

  if (!isOk) {
    return await isGranted(context);
  }

  return {
    props: { }
  }
}
