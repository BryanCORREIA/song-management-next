import { UiFileInputButton } from '../../../components/File/UiFileInputButton';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { Artist } from '../../../dto/artist.model';
import { Router, useRouter } from 'next/router';
import Link from 'next/link';
import { isGranted } from '../../../helpers/GrantedAccess';
// @ts-ignore
import cookie from 'js-cookie';

type ISongsProps  = {
  artists: Artist[];
}

export default function IndexPage(props: ISongsProps) {
  const [coverLink, setCoverLink] = useState<string>();
  const router = useRouter();
  const jwt = cookie.get('jwt');

  const addCoverHandler = async (formData: any) => {
    const config = {
      headers: { 'content-type': 'multipart/form-data' }
    };

    const file = formData.get('coverFile');

    await axios.post('/api/upload/cover', formData, config).then(async () => {
      setCoverLink(`/covers/${file.name}`);
    });
  };

  const addAlbumHandler = async (event: any) => {
    event.preventDefault();

    const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}albums`, {
      body: JSON.stringify({
        title: event.target.title.value,
        year: parseInt(event.target.year.value),
        cover: coverLink,
        artist: event.target.artist.value,
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + jwt
      },
      method: 'POST'
    });

    await res.json().then((res) => {
      router.push('/administration/albums');
    });
  }

  useEffect(() => {}, []);

  return (
    <div>
      <h1>Ajout d'un album</h1>
      <div className="btn_admin_gestion">
        <Link href="/administration/albums">
          <a className="btn_to_panel"><i className="fad fa-arrow-left"></i> Retour à la liste des albums</a>
        </Link>
      </div>
      <div className="section_add">
        <h2>Informations de l'album</h2>
        <div className="form_add">
          { !coverLink &&
            <UiFileInputButton
              onChange={addCoverHandler}
              label={"Ajouter la photo de la pochette"}
              uploadFileName={"coverFile"}
              acceptedFileTypes={"image/*"}/>
          }
          {coverLink &&
            <div className="img_cover">
              <img src={coverLink} alt=""/>
            </div>
          }

          <form onSubmit={addAlbumHandler}>
            <div className="form_section_flex">
              <input type="text" placeholder="Nom de l'album" name="title" className="form_input_70"/>
              <input type="text" placeholder="Année" name="year" className="form_input_30"/>
            </div>
            <div className="form_section_flex">
              <select name="artist" id="">
                <option value="" disabled={true} selected={true}>Artiste</option>
                {props.artists.map((artist: Artist, index: number) => (
                  <option key={index} value={artist.id}>{ artist.name }</option>
                ))}
              </select>
            </div>
            <div className="form_section_flex justify_content_flex_end">
              <input type="submit" value="Ajouter"/>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export async function getServerSideProps(context: any) {
  const { isOk } = await isGranted(context);

  if (!isOk) {
    return await isGranted(context);
  }

  const reqArtist = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}artists`);
  const artists: Artist[] = await reqArtist.json();

  return {
    props: { artists },
  }
}
