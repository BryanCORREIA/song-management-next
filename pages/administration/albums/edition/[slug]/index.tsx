import axios from 'axios';
import { useEffect, useState } from 'react';
import { Router, useRouter } from 'next/router';
import { Artist } from '../../../../../dto/artist.model';
import { Album } from '../../../../../dto/album.model';
import Link from 'next/link'
import { isGranted } from '../../../../../helpers/GrantedAccess';
// @ts-ignore
import cookie from 'js-cookie';

type IAlbumProps  = {
  album: Album;
  artists: Artist[];
}

export default function EditPage(props: IAlbumProps) {
  const [coverLink, setCoverLink] = useState<string>(props.album.cover);
  const router = useRouter();
  const jwt = cookie.get('jwt');

  const addAlbumHandler = async (event: any) => {
    event.preventDefault();

    const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}albums`, {
      body: JSON.stringify({
        id: props.album.id,
        title: event.target.title.value,
        year: parseInt(event.target.year.value),
        cover: coverLink,
        artist: event.target.artist.value,
        slug: props.album.slug
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + jwt
      },
      method: 'PATCH'
    });

    await res.json().then((res) => {
      router.push('/administration/albums');
    });
  }

  return (
    <div>
      <h1>Edition d'un album</h1>
      <div className="btn_admin_gestion">
        <Link href="/administration/albums">
          <a className="btn_to_panel"><i className="fad fa-arrow-left"></i> Retour à la liste des albums</a>
        </Link>
      </div>
      <div className="section_add">
        <h2>Informations de l'album</h2>
        <div className="form_add">
          <div className="img_cover">
            <img src={coverLink} alt=""/>
          </div>

          <form onSubmit={addAlbumHandler}>
            <div className="form_section_flex">
              <input type="text" placeholder="Nom de l'album" name="title" className="form_input_70" defaultValue={props.album.title}/>
              <input type="text" placeholder="Année" name="year" className="form_input_30" defaultValue={props.album.year}/>
            </div>
            <div className="form_section_flex">
              <select name="artist" id="">
                <option value="" disabled={true} selected={true}>Artiste</option>
                {props.artists.map((artist: Artist, index: number) => (
                  <option key={index} value={artist.id} selected={artist.id == props.album.artist.id}>{ artist.name }</option>
                ))}
              </select>
            </div>
            <div className="form_section_flex justify_content_flex_end">
              <input type="submit" value="Enregistrer"/>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export async function getServerSideProps(context: any) {
  const { isOk } = await isGranted(context);

  if (!isOk) {
    return await isGranted(context);
  }

  const slug = context.query.slug;

  const albumReq = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}albums/${slug}`);
  const album : Album = await albumReq.json();
  if (!album) {
    return {
      redirect: {
        destination: '/administration/albums',
        permanent: false,
      },
    }
  }

  const reqArtist = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}artists`);
  const artists: Artist[] = await reqArtist.json();

  return {
    props: { album, artists },
  }
}
