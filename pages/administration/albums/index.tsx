import { Album } from '../../../dto/album.model';
import { Song } from '../../../dto/song.model';
import { Style } from '../../../dto/style.model';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { Artist } from '../../../dto/artist.model';
import Link from 'next/link';
import { isGranted } from '../../../helpers/GrantedAccess';
// @ts-ignore
import cookie from 'js-cookie';

type IAlbumsProps  = {
  albums: Album[];
}

export default function Albums(props : IAlbumsProps) {
  const [listAlbum, setListAlbum] = useState<Album[]>(props.albums);
  const jwt = cookie.get('jwt');

  async function loadAlbums() {
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}albums`);
    setListAlbum(await res.json());
  }

  async function deleteAlbum(id: string) {
    await fetch(`${process.env.NEXT_PUBLIC_API_HOST}albums/${id}`, {
      headers: {
        'Authorization': 'Bearer ' + jwt
      },
      method: 'DELETE'
    }).then(() => {
      loadAlbums();
    });
  }

  useEffect(() => {}, [listAlbum]);

  return (
    <div>
      <h1>Gestion des Albums</h1>
      <div className="btn_admin_gestion">
        <Link href="/administration">
          <a className="btn_to_panel"><i className="fad fa-arrow-left"></i> Retour au panneau de gestion</a>
        </Link>
        <Link href="/administration/albums/creation">
          <a className="btn_to_add"><i className="fad fa-plus"></i> Ajouter un album</a>
        </Link>
      </div>
      <div className="table_container">
        <table className="table_admin">
          <thead>
          <tr>
            <th>Titre</th>
            <th>Artiste</th>
            <th>Année</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
          {listAlbum.map((album: Album, index: number)=> (
            <tr key={index}>
              <td>{ album.title }</td>
              <td>{ album.artist.name }</td>
              <td>{ album.year }</td>
              <td>
                <Link href={`/administration/albums/edition/${album.slug}`}>
                  <span className="btn-edit"><i className="fal fa-pen"></i></span>
                </Link>
                <span className="btn-delete" onClick={async () => await deleteAlbum(album.id)}><i className="far fa-trash"></i></span>
              </td>
            </tr>
          ))}
          <tr>
          </tr>
          </tbody>
        </table>
      </div>
    </div>
  )
}


export async function getServerSideProps(context: any) {
  const { isOk } = await isGranted(context);

  if (!isOk) {
    return await isGranted(context);
  }

  const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}albums`);
  const albums : Album[] = await res.json();

  return {
    props: { albums },
  }
}
