import { UiFileInputButton } from '../../../components/File/UiFileInputButton';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { Song } from '../../../dto/song.model';
import { Style } from '../../../dto/style.model';
import { Album } from '../../../dto/album.model';
import Link from 'next/link';
import { isGranted } from '../../../helpers/GrantedAccess';
// @ts-ignore
import cookie from 'js-cookie';

type ISongsProps  = {
  styles: Style[];
  albums: Album[];
}

export default function IndexPage(props: ISongsProps) {
  const [isUploading, setIsUploading] = useState<boolean>(false);
  const [uploadProgress, setUploadProgress] = useState<number>(0);
  const [songDuration, setSongDuration] = useState<number>();
  const [stepUpload, setStepUpload] = useState<boolean>(false);
  const [fileUpload, setFileUpload] = useState<boolean>(false);
  const [idSong, setIdSong] = useState<number>();

  const jwt = cookie.get('jwt');

  const onSongHandler = async (formData: any) => {
    setIsUploading(true);

    const config = {
      headers: { 'content-type': 'multipart/form-data' },
      onUploadProgress: (event: { loaded: number; total: number; }) => {
        setUploadProgress(Math.round((event.loaded * 100) / event.total))
      },
    };

    const objectUrl = URL.createObjectURL(formData.get('songFile'));

    const audio = new Audio(objectUrl);
    audio.preload = "metadata";

    audio.onloadedmetadata = async () => {
      await axios.post('/api/upload/song', formData, config).then(async () => {
        setFileUpload(true);

        const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}songs/${idSong}`, {
          body: JSON.stringify({
            src: `songs/${formData.get('songFile').name}`,
            duration: Math.floor(audio.duration),
          }),
          headers: {
            'Content-Type': 'application/json'
          },
          method: 'POST'
        });

        await res.json();
      });
    }
  };

  const addSongHandler = async (event: any) => {
    event.preventDefault();

    const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}songs`, {
      body: JSON.stringify({
        title: event.target.title.value,
        album: event.target.album.value,
        style: event.target.style.value
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + jwt
      },
      method: 'POST'
    });

    await res.json().then((res) => {
      setIdSong(res.id);
      setStepUpload(true);
    });
  }

  useEffect(() => {}, [isUploading, uploadProgress]);

  return (
    <div>
      <h1>Ajout d'une musique</h1>
      <div className="btn_admin_gestion">
        <Link href="/administration/musiques">
          <a className="btn_to_panel"><i className="fad fa-arrow-left"></i> Retour à la liste des musiques</a>
        </Link>
      </div>
      <div className="container_form_add_song">
        <div className="form_add_song">
          <h2 className="title_white">Information de la musique</h2>
          <form className="form_add_song_form" onSubmit={addSongHandler}>
            <div className="form_w-100">
              <input type="text" placeholder="Titre de la musique" name="title" disabled={stepUpload}/>
            </div>
            <div className="form_w-50">
              <select name="album" disabled={stepUpload}>
                <option value="" selected disabled>Album</option>
                { props.albums.map((album: Album, index: number) => (
                  <option value={album.id} key={index}>{album.title}</option>
                ))}
              </select>
              <select name="style" disabled={stepUpload}>
                <option value="" selected disabled>Genre Musical</option>
                { props.styles.map((style: Style, index: number) => (
                  <option value={style.id} key={index}>{style.name}</option>
                ))}
              </select>
            </div>

            <div className="btn_submit_form">
              <input type="submit" value="Ajouter"/>
            </div>
          </form>
        </div>

        {stepUpload &&
          <div className="form_upload">
            <h2>Ajouter le fichier audio</h2>
            <UiFileInputButton
              label="Ajouter le fichier"
              uploadFileName="songFile"
              onChange={onSongHandler}
            />

            { fileUpload && <p className="success_text">
              La bande musicale a bien été ajoutée.
            </p>}
          </div>
        }
      </div>
    </div>

  );
};

export async function getServerSideProps(context: any) {
  const { isOk } = await isGranted(context);

  if (!isOk) {
    return await isGranted(context);
  }

  const reqStyle = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}styles`);
  const styles: Style[] = await reqStyle.json();

  const reqAlbum = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}albums`);
  const albums: Album[] = await reqAlbum.json();

  return {
    props: { styles, albums },
  }
}
