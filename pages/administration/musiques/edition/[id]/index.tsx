import axios from 'axios';
import { useEffect, useState } from 'react';
import { Song } from '../../../../../dto/song.model';
import { Style } from '../../../../../dto/style.model';
import { Album } from '../../../../../dto/album.model';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { isGranted } from '../../../../../helpers/GrantedAccess';
// @ts-ignore
import cookie from 'js-cookie';

type ISongsProps  = {
  song: Song;
  styles: Style[];
  albums: Album[];
}

export default function EditPage(props: ISongsProps) {
  const routeur = useRouter();
  const jwt = cookie.get('jwt');

  const addSongHandler = async (event: any) => {
    event.preventDefault();

    const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}songs`, {
      body: JSON.stringify({
        id: props.song.id,
        title: event.target.title.value,
        album: event.target.album.value,
        style: event.target.style.value
      }),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + jwt

      },
      method: 'PATCH'
    });

    await res.json().then((res) => {
      routeur.push('/administration/musiques')
    });
  }

  return (
    <div>
      <h1>Edition d'une musique</h1>
      <div className="btn_admin_gestion">
        <Link href="/administration/musiques">
          <a className="btn_to_panel"><i className="fad fa-arrow-left"></i> Retour à la liste des musiques</a>
        </Link>
      </div>
      <div className="container_form_add_song">
        <div className="form_add_song form_add_song_center">
          <h2 className="title_white">Informations de la musique</h2>
          <form className="form_add_song_form" onSubmit={addSongHandler}>
            <div className="form_w-100">
              <input type="text" placeholder="Titre de la musique" name="title" defaultValue={props.song.title}/>
            </div>
            <div className="form_w-50">
              <select name="album">
                <option value="" selected disabled>Album</option>
                { props.albums.map((album: Album, index: number) => (
                  <option value={album.id} key={index} selected={album.id == props.song.album.id}>{album.title}</option>
                ))}
              </select>
              <select name="style">
                <option value="" selected disabled>Genre Musical</option>
                { props.styles.map((style: Style, index: number) => (
                  <option value={style.id} key={index} selected={props.song.style != null && style.id == props.song.style.id}>{style.name}</option>
                ))}
              </select>
            </div>

            <div className="btn_submit_form">
              <input type="submit" value="Enregistrer"/>
            </div>
          </form>
        </div>
      </div>
    </div>

  );
};

export async function getServerSideProps(context: any) {
  const { isOk } = await isGranted(context);

  if (!isOk) {
    return await isGranted(context);
  }

  const reqStyle = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}styles`);
  const styles: Style[] = await reqStyle.json();

  const reqAlbum = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}albums`);
  const albums: Album[] = await reqAlbum.json();

  const songReq = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}songs/${context.query.id}`);
  const song : Song = await songReq.json();
  if (!song) {
    return {
      redirect: {
        destination: '/administration/musiques',
        permanent: false,
      },
    }
  }

  return {
    props: { styles, albums, song },
  }
}
