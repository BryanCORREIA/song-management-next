import { Album } from '../../../dto/album.model';
import { Song } from '../../../dto/song.model';
import { Style } from '../../../dto/style.model';
import Link from 'next/link';
import { useState } from 'react';
import { Artist } from '../../../dto/artist.model';
import { isGranted } from '../../../helpers/GrantedAccess';
// @ts-ignore
import cookie from 'js-cookie';

type ISongsProps  = {
  songs: Song[];
}

export default function Musiques(props : ISongsProps) {
  const [listSong, setListSong] = useState<Song[]>(props.songs);
  const jwt = cookie.get('jwt');

  async function loadSongs() {
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}artists`);
    setListSong(await res.json());
  }

  async function deleteSong(id: number) {
    await fetch(`${process.env.NEXT_PUBLIC_API_HOST}songs/${id}`, {
      headers: {
        'Authorization': 'Bearer ' + jwt
      },
      method: 'DELETE'
    }).then(() => {
      loadSongs();
    });
  }
  return (
    <div>
      <h1>Gestion des musiques</h1>
      <div className="btn_admin_gestion">
        <Link href="/administration">
          <a className="btn_to_panel"><i className="fad fa-arrow-left"></i> Retour au panneau de gestion</a>
        </Link>
        <Link href="/administration/musiques/creation">
          <a className="btn_to_add"><i className="fad fa-plus"></i> Ajouter une musique</a>
        </Link>
      </div>
      <div className="table_container">
        <table className="table_admin">
          <thead>
          <tr>
            <th>Nom</th>
            <th>Auteur</th>
            <th>Genre</th>
            <th>Album</th>
            <th>Temps</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody>
          {listSong.map((song: Song, index: number)=> (
            <tr key={index}>
              <td>{ song.title }</td>
              <td>{ song.album.artist.name }</td>
              <td>
                { song.style != null &&
                  <span className="span_style_name">{ song.style.name }</span>
                }
              </td>
              <td>{ song.album.title }</td>
              <td>{ song.duration }</td>
              <td>
                <Link href={`/administration/musiques/edition/${song.id}`}>
                  <span className="btn-edit"><i className="fal fa-pen"></i></span>
                </Link>
                <span className="btn-delete" onClick={() => deleteSong(song.id)}><i className="far fa-trash"></i></span>
              </td>
            </tr>
          ))}
          <tr>
          </tr>
          </tbody>
        </table>
      </div>
    </div>
  )
}


export async function getServerSideProps(context: any) {
  const { isOk } = await isGranted(context);

  if (!isOk) {
    return await isGranted(context);
  }

  const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}songs`);
  const songs : Song[] = await res.json();

  return {
    props: { songs }, // will be passed to the page component as props
  }
}
