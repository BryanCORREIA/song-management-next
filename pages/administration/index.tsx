import Link from 'next/link';
import { Song } from '../../dto/song.model';
import cookies from 'next-cookies';
import { isGranted } from '../../helpers/GrantedAccess';

export default function Administration() {
  return (
    <div>
      <h1>Panneau de gestion</h1>
      <div className="list_button_admin">
        <Link href="/administration/albums">
          <div className="btn_admin_panel">
            <div className="icon_btn_admin">
              <i className="fad fa-album-collection"></i>
            </div>
            Albums
          </div>
        </Link>
        <Link href="/administration/artistes">
          <div className="btn_admin_panel">
            <div className="icon_btn_admin">
              <i className="fad fa-user-music"></i>
            </div>
            Artistes
          </div>
        </Link>
        <Link href="/administration/musiques">
          <div className="btn_admin_panel">
            <div className="icon_btn_admin">
              <i className="fad fa-music"></i>
            </div>
            Musiques
          </div>
        </Link>
      </div>
    </div>
  )
}

export async function getServerSideProps(context: any) {
  const { isOk } = await isGranted(context);

  if (!isOk) {
    return await isGranted(context);
  }

  return {
    props: { }, // will be passed to the page component as props
  }
}


