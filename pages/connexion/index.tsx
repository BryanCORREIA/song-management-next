import { useEffect, useState } from 'react';
import Router from 'next/router';
import { useCookies } from "react-cookie";

export default function Connexions() {
  const [formError, setFormError] = useState<boolean>(false);
  const [cookie, setCookie] = useCookies(["jwt"]);

  const login = async (event: any) => {
    event.preventDefault();

    const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}auth/login`, {
      body: JSON.stringify({
        username: event.target.username.value,
        password: event.target.password.value
      }),
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST'
    });

    const result = await res.json();

    if (result.statusCode == 401) {
      setFormError(true);
      event.target.password.value = "";
    } else if (result.access_token) {
      setFormError(false);
      setCookie("jwt", result.access_token, {
        path: "/",
        maxAge: 3600, // Expires after 1hr
        sameSite: true,
      });
      await Router.replace(window.location.origin);
    }
  };

  return (
    <div>
      <h1>Connexion</h1>
      <form onSubmit={login}>
        <label htmlFor="username">Adresse Mail</label>
        <input type="text" name="username" id="username" autoComplete="username" required/>

        <label htmlFor="password">Mot de passe</label>
        <input type="password" name="password" id="password" required/>
        <button type="submit">login</button>
      </form>
      {formError &&
        <p>Mdr tu connais pas tes identifiants.</p>
      }
    </div>
  )
}

