import { Router, useRouter } from 'next/router';
// @ts-ignore
import cookie from 'js-cookie';
import { Song } from '../../../dto/song.model';
import cookies from 'next-cookies';
import { useRecoilState } from 'recoil';
import { userConnected } from '../../../components/userConnected.state';
import { useEffect } from 'react';

export default function PlaylistId(props: any) {
  const jwt = cookie.get('jwt');
  const [userSession, setUserSession] = useRecoilState(userConnected);

  const router = useRouter();

  useEffect(() => {
    if(!jwt) {
      router.push('/401');
    }
  }, [userSession, jwt]);

  return (
    <div>
      {jwt &&
        <div>
          <h1 className="title-playlist">{props.playlist.name}</h1>
          <hr/>
          <div className="header-playlist">
            <h2>Liste des musiques de votre playlist</h2>
            <button><i className="fad fa-trash"></i></button>
          </div>
          <div className="list-songs-playlist">
            {!props.playlist.any &&
            <p className="no-songs">Vous n'avez ajouté aucune chanson à cette playlist !</p>
            }
            {props.playlist.any && props.playlist.songs.map((song: Song, index: number) => (
              <div key={index}>
                {index} {song.title} {song.album.artist.name} {song.album.title} {song.duration} <button>play</button> <button>delete</button>
              </div>
            ))}
          </div>
        </div>
      }
    </div>
  )
}

export async function getServerSideProps(context: any) {
  let playlist = { };
  const { jwt } = cookies(context);

  const playlistReq = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}playlist/${context.query.slug}`, {
    method: 'GET',
    headers: new Headers({
      'Authorization': 'Bearer ' + jwt
    })
  });

  let status = playlistReq.status;

  if (status === 401) {
    return {
      redirect: {
        source: '/playlist/${context.query.id}',
        destination: '/401',
        permanent: false
      }
    }
  }

  playlist = await playlistReq.json();

  return {
    props: {
      playlist: playlist,
    }
  }
}

