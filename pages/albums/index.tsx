import { Album } from '../../dto/album.model';
import ListAlbumsComponent from '../../components/Albums/list.component';

type IAlbumsProps  = {
  albums: Album[];
}

export default function Albums(props : IAlbumsProps) {
  return (
    <div>
      <h1>Albums</h1>
      <ListAlbumsComponent
        albums={props.albums}
        selectedAlbum={null}
      />
    </div>
  )
}

export async function getServerSideProps(context: any) {
  const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}albums`);
  const albums : Album[] = await res.json();
  if (!albums) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }
  return {
    props: { albums }, // will be passed to the page component as props
  }

}

