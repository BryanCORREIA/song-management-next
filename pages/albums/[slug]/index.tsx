import ListAlbumsComponent from '../../../components/Albums/list.component';
import { Album } from '../../../dto/album.model';

type IAlbumsProps  = {
  albums: Album[];
  album: Album
}

export default function Albums(props : IAlbumsProps) {
  return (
    <div>
      <h1>Albums</h1>
      <ListAlbumsComponent
        albums={props.albums}
        selectedAlbum={props.album}
      />
    </div>
  )
}

export async function getServerSideProps(context: any) {
  const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}albums`);
  const albums : Album[] = await res.json();
  if (!albums) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }

  const selectedAlbum = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}albums/${context.query.slug}`);
  const album : Album = await selectedAlbum.json();
  if (!album) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }

  return {
    props: {
      albums,
      album,
    },
  }

}

