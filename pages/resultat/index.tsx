import cookies from 'next-cookies';
import { Search } from '../../dto/search.model';
import { Album } from '../../dto/album.model';
import { Playlist } from '../../dto/playlist.model';
import { Song } from '../../dto/song.model';
import { Artist } from '../../dto/artist.model';

type ISearchProps  = {
  search: string;
  result: Search;
}

export default function Albums(props : ISearchProps) {
  return (
    <div>
      <h1>Résultat pour : { props.search }</h1>
      <h2>Albums</h2>
      {props.result.albums.map((album: Album, index: number) => (
        <span>{album.title}</span>
      ))}
      <h2>Musiques</h2>
      {props.result.songs.map((song: Song, index: number) => (
        <span>{song.title}</span>
      ))}
      <h2>Artistes</h2>
      {props.result.artists.map((artist: Artist, index: number) => (
        <span>{artist.name}</span>
      ))}
    </div>
  )
}

export async function getServerSideProps(context: any) {
  const search = context.query.recherche;
  const { jwt } = cookies(context);

  if (!search) {
    return {
      redirect: {
        destination: '/',
        permanent: false,
      },
    }
  }

  const searchQuery = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}search`, {
    body: JSON.stringify({
      query: search,
    }),
    headers: {
      'Content-Type': 'application/json'
    },
    method: 'POST'
  })

  const result = await searchQuery.json();

  return {
    props: {
      search,
      result
    },
  }
}

