import '../styles/globals.css'
import { AppProps } from 'next/app'
import LayoutBase from '../components/Layout/LayoutBase';
import { RecoilRoot } from 'recoil';
import { CookiesProvider } from "react-cookie"

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <CookiesProvider>
      <RecoilRoot>
        <LayoutBase>
          <Component {...pageProps} />
        </LayoutBase>
      </RecoilRoot>
    </CookiesProvider>
  )
}
export default MyApp;

