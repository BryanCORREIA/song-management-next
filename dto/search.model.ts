import { Song } from './song.model';
import { Album } from './album.model';
import { Artist } from './artist.model';
import { Playlist } from './playlist.model';

export interface Search {
  songs: Song[];
  albums: Album[];
  artists: Artist[];
  playlists: Playlist[];
}
