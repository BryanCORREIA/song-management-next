import { Album } from './album.model';

export interface Artist {
  id: string;
  name: string;
  albums: Album[];
}
