import { Album } from './album.model';
import { Style } from './style.model';

export interface Song {
  id: number;
  title: string;
  duration: number;
  src: string;
  style: Style;
  album: Album;
}
