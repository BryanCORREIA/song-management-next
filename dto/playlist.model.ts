import { Song } from './song.model';

export interface Playlist {
  id: string;
  slug: string;
  name: string;
  songs: Song[];
}
