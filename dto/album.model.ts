import { Song } from './song.model';
import { Artist } from './artist.model';

export interface Album {
  id: string;
  slug: string;
  title: string;
  year: number;
  cover: string;
  artist: Artist;
  songs: Song[];
}
