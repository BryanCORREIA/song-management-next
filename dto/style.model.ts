import { Song } from './song.model';

export interface Style {
  id: string;
  slug: string;
  name: string;
  songs: Song[];
}
