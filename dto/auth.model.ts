export interface Auth {
  access_token: string;
  exp: string;
}
