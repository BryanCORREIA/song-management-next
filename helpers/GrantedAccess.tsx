import cookies from 'next-cookies';

export async function isGranted(context: any) {
  const { jwt } = cookies(context);

  const res = await fetch(`${process.env.NEXT_PUBLIC_API_HOST}user/profil`, {
    method: 'GET',
    headers: new Headers({
      'Authorization': 'Bearer ' + jwt
    })
  });

  const respUser = await res;

  // @ts-ignore
  if (respUser.statusCode === 401) {
    return {
      redirect: {
        source: '/administration',
        destination: '/'
      }
    }
  }
  else {
    const user = await respUser.json();

    if (user.role != "ROLE_ADMIN") {
      return {
        redirect: {
          source: '/administration',
          destination: '/'
        }
      }
    }
  }

  return {
    isOk: true
  }
}
